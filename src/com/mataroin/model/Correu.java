package com.mataroin.model;
/* hay tres clases de JavaMail implicadas:
    -Session: De alguna forma representa la conexi�n con el servidor gmail de correo. 
    Hay que obtenerla pas�ndole los par�metros de configuraci�n para el servidor de correo.
    -Transport: Clase para el env�o de mensajes. Se obtiene llamando al m�todo getTransport() de la clase Session.
    -MimeMessage: El mensaje. */
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Correu {

	public static void enviarMail(String destinatari, String missatge){
        // Propiedades de la conexi�n
        Properties properties = new Properties();
        
	    properties.put("mail.smtp.host", "smtp.gmail.com");
	    properties.put("mail.smtp.starttls.enable", "true");
	    properties.put("mail.smtp.port", "587");
	    properties.put("mail.smtp.mail.sender", "crcxapu@gmail.com");
	    properties.put("mail.smtp.password", "java2015");
	    properties.put("mail.smtp.user", "crcxapu");
	    properties.put("mail.smtp.auth", true);
	    
	    try{   // Preparamos la sesion
            Session session = Session.getDefaultInstance(properties);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("crcxapu@gmail.com"));
            message.addRecipient(
                Message.RecipientType.TO,
                new InternetAddress(destinatari));
            message.setSubject("MataroIn");
            message.setText(missatge);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("crcxapu@gmail.com", "java2015");
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
