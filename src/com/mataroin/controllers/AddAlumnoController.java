package com.mataroin.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mataroin.business.AlumnoBu;
import com.mataroin.business.IBu;
import com.mataroin.model.Alumno;
import com.mataroin.model.Correu;


/**
 * Servlet implementation class AlumnoControllers
 */
@WebServlet("/AddAlumnoController")
public class AddAlumnoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	static final Logger logger = LogManager.getLogger(AddAlumnoController.class);
    
	private IBu<Alumno> iBu=null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddAlumnoController() {
        super();
        iBu=new AlumnoBu();
    	logger.trace("entrem a AddAlumnoController()");
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace("entrem a doGet");
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace("entrem a doPost");
		
		ServletContext context=getServletContext();
		RequestDispatcher rdispatcher=context.getRequestDispatcher("/MostrarAlumnoInsertado.jsp");
		PrintWriter out = response.getWriter();
		
		if ((request.getParameter("nombre") != null) && 
				(request.getParameter("apellidos") != null) &&
				(request.getParameter("correo") != null) &&
				(request.getParameter("privacidad") != null)){			
			try {
				int id = iBu.add(omplirAlumne(request));
				Correu.enviarMail(request.getParameter("correo"), "afegit a la BDD");
				logger.info("el id insertat " + Integer.toString(id));
				out.println("<h1 class='h1Sortida'>Alumne insertat correctament</h1>");
				logger.debug("Alumne insertat correctament");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				out.println("<h1 class='h1Sortida'>Error en la inserci�</h1>");
				logger.debug("Error en la inserci");
			}			
		} else {
			out.println("<h1 class='h1Sortida'>Falten camps opbligatoris</h1>");
			logger.debug("Falten camps opbligatoris");
		}
		rdispatcher.include(request, response);			
	}

	private Alumno omplirAlumne(HttpServletRequest request){
		// instancia de alumno
		Alumno alumno =new Alumno();
		alumno.setNombre(request.getParameter("nombre"));
		alumno.setApellidos(request.getParameter("apellidos"));
		alumno.setEmpresa(request.getParameter("empresa"));
		alumno.setTelefono(request.getParameter("telefono"));
		alumno.setEmail(request.getParameter("correo"));
		alumno.setCiudad(request.getParameter("ciudad"));
		alumno.setComentario(request.getParameter("comentario"));
		return alumno;
	}
}
