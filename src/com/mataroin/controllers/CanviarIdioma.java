package com.mataroin.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.PropertyResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;
/**
 * Servlet implementation class CanviarIdioma
 */
@WebServlet("/CanviarIdioma")
public class CanviarIdioma extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(CanviarIdioma.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CanviarIdioma() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace("entrem a doPost CanviarIdioma.servlet");
		final String langEtiquetes = "com.mataroin.multiidioma.Etiquetes"; 
		PropertyResourceBundle etiquetes=null;	
		
		if (request.getParameter("hdIdioma")!=null){
			switch(Integer.parseInt(request.getParameter("hdIdioma"))){
			case 0: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
					break;
			case 1: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("es", "ES"));
					break;
			case 2: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("en", "GB"));
					break;
			case 3: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("fr", "FR"));
					break;
			default: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
					break;
			}
		}else {
			etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
		}
		
		ServletContext context=getServletContext();
		RequestDispatcher rdispatcher=context.getRequestDispatcher("/MostrarAlumnosController");
		request.setAttribute("etiquetes", etiquetes);
		logger.trace("ara farem el forward a MostrarAlumnosControler.servlet");
		rdispatcher.forward(request, response);
		logger.trace("fet el forward MostrarAlumnosControler.servlet");
	}

}
