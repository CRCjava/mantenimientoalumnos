package com.mataroin.controllers;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mataroin.business.AlumnoBu;
import com.mataroin.business.IBu;
import com.mataroin.model.Alumno;

/**
 * Servlet implementation class MostrarAlumno
 */
@WebServlet("/MostrarAlumnos")
public class MostrarAlumnosController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(MostrarAlumnosController.class);
	private IBu<Alumno> iBu=null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MostrarAlumnosController() {
        super();
        iBu=new AlumnoBu();
        logger.trace("entrem a MostrarAlumnosController()");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace("entrem a doGet");
		List<Alumno> llistaAlumnes;
		try {
			llistaAlumnes = iBu.get();			
			logger.trace("ara hem fet el iBu.get()");
			ServletContext context=getServletContext();
			RequestDispatcher rdispatcher=context.getRequestDispatcher("/MostrarAlumnos.jsp");
			request.setAttribute("llistaAlumnes", llistaAlumnes);
			logger.trace("ara farem el forward doGet");
			rdispatcher.forward(request, response);
			logger.trace("fet el forward doGet");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace("entrem a doPost i fem un rediccionament a doGet");
		doGet(request, response);
	}
}
