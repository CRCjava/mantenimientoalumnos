package com.mataroin.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mataroin.business.AlumnoBu;
import com.mataroin.business.IBu;
import com.mataroin.model.Alumno;

/**
 * Servlet implementation class InsertarAlumnoController
 */
@WebServlet("/InsertarAlumnoController")
public class InsertarAlumnoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(InsertarAlumnoController.class);
	private IBu<Alumno> iBu=null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertarAlumnoController() {
        super();
        iBu=new AlumnoBu();
        logger.trace("entrem a InsertarAlumnoController()");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace("en el doGet");		
		ServletContext context=getServletContext();
		RequestDispatcher rdispatcher=context.getRequestDispatcher("/InsertarAlumno.jsp");
		logger.trace("ara farem el forward doGet");
		rdispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
