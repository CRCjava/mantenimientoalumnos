package com.mataroin.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mataroin.business.AlumnoBu;
import com.mataroin.business.IBu;
import com.mataroin.business.IbuscarBu;
import com.mataroin.model.Alumno;

/**
 * Servlet implementation class ActualizarAlumnoController
 */
@WebServlet("/ActualizarAlumnoController")
public class ActualizarAlumnoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(ActualizarAlumnoController.class);
	private IbuscarBu<Alumno> ibuscarBu=null;
	private IBu<Alumno> iBu=null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActualizarAlumnoController() {
        super();
        ibuscarBu=new AlumnoBu();
        logger.trace("entrem a ActualizarAlumnosController()");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace("entrem a doGet");
		Alumno alumno;
		ServletContext context=getServletContext();
		RequestDispatcher rdispatcher=rdispatcher=context.getRequestDispatcher("/MostrarAlumnos");;
		try {
			logger.trace("entrem a try");
			if (request.getParameter("accio") == "mod"){
				alumno = ibuscarBu.getById(Integer.parseInt(request.getParameter("idInteresado")));
				//alumno=ibuscarBu.getById(26);
				logger.trace("ara hem fet el ibuscarBu.get(), hem recuperat l'alumne: " + alumno.getApellidos());
				
				rdispatcher=context.getRequestDispatcher("/ActualizarAlumno.jsp");
				request.setAttribute("alumno", alumno);
			}else{ if (request.getParameter("accio") == "del"){
				logger.trace("ara esborrarem l'alumne: " + request.getParameter("idInteresado"));
				int filaEsborrada=iBu.remove(Integer.parseInt(request.getParameter("idInteresado")));
				logger.trace("s'ha esborrat: " + filaEsborrada + " files");
				rdispatcher=context.getRequestDispatcher("/MostrarAlumnos");
			}}
			rdispatcher.forward(request, response);
			logger.trace("fet el forward doGet");
		} catch (Exception e) {
			logger.error("ActualizarAlumnoController doGet " + e.getMessage());
			e.printStackTrace();
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		iBu=new AlumnoBu();
		Alumno alumno=new Alumno();
		alumno.setIdInteresado(Integer.parseInt(request.getParameter("idInteresado")));
		alumno.setNombre(request.getParameter("nombre"));
		alumno.setApellidos(request.getParameter("apellidos"));
		alumno.setCiudad(request.getParameter("ciudad"));
		alumno.setComentario(request.getParameter("comentario"));
		alumno.setEmail(request.getParameter("correo"));
		alumno.setEmpresa(request.getParameter("empresa"));
		alumno.setTelefono(request.getParameter("telefono"));
		try {
			iBu.modify(alumno);
		} catch (Exception e) {
			logger.error("ActualizarAlumnoController doPost " + e.getMessage());
			e.printStackTrace();
		}
		logger.trace("ara hem fet el iBu.modify() l'alumne: " + request.getParameter("idInteresado"));
		ServletContext context=getServletContext();
		RequestDispatcher rdispatcher=context.getRequestDispatcher("/MostrarAlumnos");
		rdispatcher.forward(request, response);
	}

}
