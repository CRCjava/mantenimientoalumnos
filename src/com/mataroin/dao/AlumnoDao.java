package com.mataroin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mataroin.model.Alumno;

public class AlumnoDao extends BaseDao implements IDao<Alumno>,IbuscarDao<Alumno>{

	private final String sqlInsert="insert into interesados(nombre,apellidos,empresa,telefono,email,ciudad,comentario) values (?,?,?,?,?,?,?)";
	private final String sqlSelect="select * from interesados";
	private final String sqlUpdate="update interesados "
			+ "set nombre=?, apellidos=?, empresa=?, telefono=?, email=?, ciudad=?, comentario=? "
			+ "where idInteresado=?";
	private final String sqlRemove="delete from interesados where idInteresado = ?";
	private final String sqlById="select * from interesados where idInteresado = ?";
	private final String sqlWhere="select * from interesados where ?";
	private Connection connect=null;
	private PreparedStatement preparedStatement=null;
	
	@Override
	public int add(Alumno alumno) throws Exception{
		Connection connect=null;
		PreparedStatement preparedStatement=null;
		
		int id= 0;		
		try{
			connect=getConection();
			preparedStatement = connect.prepareStatement(sqlInsert);
			
			preparedStatement.setString(1, alumno.getNombre());
			preparedStatement.setString(2, alumno.getApellidos());
			preparedStatement.setString(3, alumno.getEmpresa());
			preparedStatement.setString(4, alumno.getTelefono());
			preparedStatement.setString(5, alumno.getEmail());
			preparedStatement.setString(6, alumno.getCiudad());
			preparedStatement.setString(7, alumno.getComentario());
			
			preparedStatement.executeUpdate();
			ResultSet keyRs=preparedStatement.getGeneratedKeys();
			if (keyRs.next()){
				id=(int) keyRs.getInt(1);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if (connect!=null) connect.close();
			if (preparedStatement!=null) preparedStatement.close();
		}
	return id;
	}

	@Override
	public int modify(Alumno alumno) throws  Exception{
		Connection connect=null;
		PreparedStatement preparedStatement=null;
		int filesModificades= 0;
		try{
			connect=getConection();
			preparedStatement = connect.prepareStatement(sqlUpdate);
			
			preparedStatement.setString(1, alumno.getNombre());
			preparedStatement.setString(2, alumno.getApellidos());
			preparedStatement.setString(3, alumno.getEmpresa());
			preparedStatement.setString(4, alumno.getTelefono());
			preparedStatement.setString(5, alumno.getEmail());
			preparedStatement.setString(6, alumno.getCiudad());
			preparedStatement.setString(7, alumno.getComentario());
			preparedStatement.setInt(8, alumno.getIdInteresado());
			
			filesModificades=preparedStatement.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if (connect!=null) connect.close();
			if (preparedStatement!=null) preparedStatement.close();
		}
	return filesModificades;
	}

	@Override
	public int remove(int id) throws  Exception{
		Connection connect=null;
		PreparedStatement preparedStatement=null;
		int filesEsborrades=0;
		
		try{
			connect=getConection();
			preparedStatement = connect.prepareStatement(sqlRemove);			
			preparedStatement.setInt(1, id);			
			filesEsborrades=preparedStatement.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if (connect!=null) connect.close();
			if (preparedStatement!=null) preparedStatement.close();
		}
		return filesEsborrades;
	}

	@Override
	public List<Alumno> get() throws Exception{
		// polimorfismo de interfaces
		List<Alumno> llistaAlumnes=new ArrayList<Alumno>();	
		Connection connect=null;
		PreparedStatement preparedStatement=null;
		try{	
			connect=getConection();	
			preparedStatement = connect.prepareStatement(sqlSelect);
			ResultSet rs = preparedStatement.executeQuery();
			deResulSetAllista(rs, llistaAlumnes);
		    preparedStatement.close();			
		}catch(Exception e){
			// log4j i mail
			throw e;
		}finally{
			if (connect!=null) connect.close();
			if (preparedStatement!=null) preparedStatement.close();
		}
		return llistaAlumnes;
	}

	@Override
	public List<Alumno> getWhere(String condicio) throws Exception {
		List<Alumno> llistaAlumnes=new ArrayList<Alumno>();	
		Connection connect=null;
		PreparedStatement preparedStatement=null;
		try{	
			connect=getConection();	
			preparedStatement = connect.prepareStatement(sqlWhere);
			preparedStatement.setString(1, condicio);
			ResultSet rs = preparedStatement.executeQuery();
			deResulSetAllista(rs, llistaAlumnes);
		    preparedStatement.close();			
		}catch(Exception e){
			// log4j i mail
			throw e;
		}finally{
			if (connect!=null) connect.close();
			if (preparedStatement!=null) preparedStatement.close();
		}
		return llistaAlumnes;
	}

	@Override
	public Alumno getById(int id) throws Exception {
		Connection connect=null;
		PreparedStatement preparedStatement=null;
		Alumno alumno=new Alumno();
		try{
			connect=getConection();
			preparedStatement = connect.prepareStatement(sqlById);
			preparedStatement.setInt(1, id);	
			ResultSet rs = preparedStatement.executeQuery();
			deResulSetAalumno(rs, alumno);		
		}catch(Exception e){
			// log4j i mail
			throw e;
		}finally{
			if (connect!=null) connect.close();
			if (preparedStatement!=null) preparedStatement.close();
		}
		return alumno;
	}
	
	void deResulSetAalumno(ResultSet rs, Alumno alumno) throws Exception{
		 while(rs.next()){		                   
			alumno.setIdInteresado(rs.getInt("idInteresado"));
	        alumno.setNombre(rs.getString("nombre"));
	        alumno.setApellidos(rs.getString("apellidos"));
	        alumno.setEmail(rs.getString("email"));
	        alumno.setEmpresa(rs.getString("empresa"));
	        alumno.setTelefono(rs.getString("telefono"));
	        alumno.setEmpresa(rs.getString("empresa"));
	        alumno.setComentario(rs.getString("comentario"));
	     }
	}
	
	// aquest metode tant el crida el get()) com el getWhere()
	void deResulSetAllista(ResultSet rs, List<Alumno> llistaAlumnes) throws Exception{
		while (rs.next()){
			// creem un objecte per cada fila
			Alumno alumno = new Alumno();
			alumno.setIdInteresado(rs.getInt("idInteresado"));
	        alumno.setNombre(rs.getString("nombre"));
	        alumno.setApellidos(rs.getString("apellidos"));
	        alumno.setEmail(rs.getString("email"));
	        alumno.setEmpresa(rs.getString("empresa"));
	        alumno.setTelefono(rs.getString("telefono"));
	        llistaAlumnes.add(alumno);
	     }
	}
}
