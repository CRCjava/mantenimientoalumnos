package com.mataroin.dao;

import java.util.List;

public interface IDao<T> {
	// en una interficie tots els seus m�todes s�n p�blics (per defecte)
	int add(T modelo) throws  Exception;
	int modify(T modelo) throws  Exception;
	int remove(int Id) throws  Exception;
	List<T> get() throws  Exception; 
}
