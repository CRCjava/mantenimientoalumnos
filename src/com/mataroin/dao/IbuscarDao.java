package com.mataroin.dao;

import java.util.List;

public interface IbuscarDao<T> {
	List<T> getWhere(String condicio) throws  Exception;
	T getById(int id) throws  Exception;
}
