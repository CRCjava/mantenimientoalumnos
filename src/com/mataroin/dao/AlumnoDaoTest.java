package com.mataroin.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mataroin.model.Alumno;

public class AlumnoDaoTest {

	@Test
	public void testAdd() throws Exception{
		Alumno alumno=new Alumno();
		alumno.setNombre("carme");
		alumno.setApellidos("riu");
		alumno.setEmail("crc@gmail.com");
		alumno.setEmpresa("atur");
		
		IDao<Alumno> iDao = new AlumnoDao();
		assertTrue(iDao.add(alumno) == 1);
	}
	
	@Test
	public void testGet() throws Exception{
		/* instancia AlumnoDao la interficie
		 * llamar al get() i ens retornes l'arrayList
		 * mirar el size de l'arrayList si > 0 -> ok
		 */
		IDao<Alumno> iDao=new AlumnoDao();
		List<Alumno> llistaAlumne= new ArrayList<Alumno>();
		llistaAlumne=iDao.get();
		
		assertTrue(llistaAlumne.size() > 1);
	}

}
