package com.mataroin.dao;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class BaseDao {
	private final String conexio="jdbc:mysql://localhost/mataroin?user=root";
	// static initializer
	static{
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	// el fem public per poder fer testing
	public Connection getConection() throws SQLException{	
		Connection connect=null;
		try{
			connect=DriverManager.getConnection(conexio);
		}catch(SQLException ex){
			ex.printStackTrace();
			throw ex;
		}
		return connect;
	}
}
