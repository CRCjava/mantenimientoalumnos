package com.mataroin.business;

import java.util.List;

import com.mataroin.dao.AlumnoDao;
import com.mataroin.dao.IDao;
import com.mataroin.dao.IbuscarDao;
import com.mataroin.model.Alumno;

public class AlumnoBu implements IBu<Alumno>, IbuscarBu<Alumno>{

	private IDao<Alumno> iDao=new AlumnoDao();
	private IbuscarDao<Alumno> iBuscarDao=new AlumnoDao();
	
	public AlumnoBu(){
		// inyecto la interfaz de alumnoDAo
		iDao = new AlumnoDao();
	}
	@Override
	public int add(Alumno alumno) throws Exception {
		return iDao.add(alumno);
	}
	@Override
	public List<Alumno> get() throws Exception {
		return iDao.get();
	}
	@Override
	public int modify(Alumno alumno) throws Exception {
		return iDao.modify(alumno);
	}
	@Override
	public int remove(int id) throws Exception {
		return iDao.remove(id);
	}
	@Override
	public List<Alumno> getWhere(String condicio) throws Exception {
		return iBuscarDao.getWhere(condicio);
	}
	@Override
	public Alumno getById(int id) throws Exception {
		return iBuscarDao.getById(id);
	}
}
