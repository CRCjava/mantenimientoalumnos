package com.mataroin.business;

import java.util.List;

public interface IbuscarBu<T>{
	List<T> getWhere(String condicio) throws  Exception;
	T getById(int id) throws  Exception;
}