package com.mataroin.business;

import java.util.List;

public interface IBu<T> {
	int add(T modelo) throws  Exception;
	int modify(T modelo) throws  Exception;
	int remove(int Id) throws  Exception;
	List<T> get() throws  Exception;	
}
