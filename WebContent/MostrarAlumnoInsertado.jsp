<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Mostrar Interessat</title>
	<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
<%@page import="java.util.PropertyResourceBundle,java.util.Locale,java.util.ResourceBundle"%>
	<% // create and load default properties
	final String langEtiquetes = "com.mataroin.multiidioma.Etiquetes"; 
	PropertyResourceBundle etiquetes=(PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("en", "GB"));
	%>
<table class="CSSTableGenerator">
<tbody>	
	<tr><td><%=etiquetes.getString("lblNom") %></td>
		<td><%=request.getParameter("nombre") %></td></tr>
		
	<tr><td><%=etiquetes.getString("lblCognoms") %></td>
		<td><%=request.getParameter("apellidos") %></td></tr>	
				
	<tr><td><%=etiquetes.getString("lblEmpresa") %></td>
		<td><%=request.getParameter("empresa") %></td></tr>
		
	<tr><td><%=etiquetes.getString("lblTelf") %></td>
		<td><%=request.getParameter("telefono") %></td></tr>
		
	<tr><td><%=etiquetes.getString("lblCorreu") %></td>
		<td><%=request.getParameter("correo") %></td></tr>
		
	<tr><td><%=etiquetes.getString("lblCiutat") %></td>
		<td><%=request.getParameter("ciudad") %></td></tr>
		
	<tr><td><%=etiquetes.getString("lblComentari") %></td>
		<td><%=request.getParameter("comentario") %></td></tr>
</tbody></table>

</body>
</html>