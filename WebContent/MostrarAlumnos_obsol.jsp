<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="com.mataroin.model.Alumno" %>
<%@ page import="org.apache.logging.log4j.LogManager" %>
<%@ page import="org.apache.logging.log4j.Logger" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mostrar Interessats</title>
<link rel="stylesheet" type="text/css" href="form.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="funcionsVaries.js"></script>
</head>
<body>
<%-- no cal, ara utilitzem els tags de JSTL
	<%! List<Alumno> llistaAlumnes=null;%>
	<% llistaAlumnes= (List<Alumno>)request.getAttribute("listaAlumnos");%>

  Using JSTL forEach and out to loop a list and display items in table --%>
<div>
<div class="columna">
	<form action='InsertarAlumnoController' method='get'>
		<button type='submit' name='enviar' id='enviar'>Afegir-ne</button>
	</form>
</div>
<div class="columna">
    <h3>Triar Idioma</h3>
</div>
<div class="columna">	
	<button type="button" name="botoIdioma" id="bt_cat" 
	onclick="$.get('MostrarAlumnos?cat', function(data, status){
		alert('hola');});">
		<img alt="bandera catalana" src="banderes/Catalonia.png"> CAT</button>
	<button type="button" name="botoIdioma" id="bt_esp" onclick="location.href='MostrarAlumnos?esp';"><img alt="bandera espa�ola" src="banderes/Spain.png"> ESP</button>
	<button type="button" name="botoIdioma" id="bt_eng" onclick="location.href='MostrarAlumnos?ang';"><img alt="bandera anglesa" src="banderes/UnitedKingdom.png"> ENG</button>
</div>

<%@page import="java.util.PropertyResourceBundle,java.util.Locale,java.util.ResourceBundle"%>
	<% // create and load default properties
	final String langEtiquetes = "com.mataroin.multiidioma.Etiquetes"; 
	PropertyResourceBundle etiquetes=(PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("en", "GB"));
	%>

<div class="columna">

 	<label><%=etiquetes.getString("lblNom") %></label>
 	<input id="buscarPerNom" name="buscarPerNom" type="text" class="inputs"/>
 	<label ><%=etiquetes.getString("lblCognoms") %></label>
 	<input id="buscarPerCognoms" name="buscarPerCognoms" type="text" class="inputs"/>
 	<label ><%=etiquetes.getString("lblCiutat") %></label>
	<input id="buscarPerCiutat" name="buscarPerCiutat" type="text" class="inputs"/>
	
	<button type="button" name="bt_buscar" id="bt_buscar">
		<img alt="lupa" src="img/lupa-16.png"> <%=etiquetes.getString("bt_buscar") %></button>
</div>
</div>

<h1>Alumnes interessats</h1>
<table class="CSSTableGenerator">
<tbody>
	<tr><th><%=etiquetes.getString("lblNom") %></th><th><%=etiquetes.getString("lblCognoms") %></th><th><%=etiquetes.getString("lblEmpresa") %></th><th><%=etiquetes.getString("lblTelf") %></th><th><%=etiquetes.getString("lblCorreu") %></th><th><%=etiquetes.getString("t_Actualitzar") %></th><th><%=etiquetes.getString("t_Esborrar") %></th></tr>
	<c:forEach var="alumno" items="${requestScope.llistaAlumnes}">
	<tr><td><c:out value="${alumno.nombre}"></c:out></td>
	<td><c:out value="${alumno.apellidos}"></c:out></td>
	<td><c:out value="${alumno.empresa}"></c:out></td>
	<td><c:out value="${alumno.telefono}"></c:out></td>
	<td><c:out value="${alumno.email}"></c:out></td>
	<td>faltaAct</td>
	<td>faltaEsborr</td></tr>
	</c:forEach>
</tbody>
</table>
<script src="formValidacion.js"></script>
</body>
</html>