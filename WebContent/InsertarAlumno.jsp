<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.PropertyResourceBundle,java.util.Locale,java.util.ResourceBundle"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insertar Interessat</title>
	<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
	<% // create and load default properties
	final String langEtiquetes = "com.mataroin.multiidioma.Etiquetes"; 
	PropertyResourceBundle etiquetes=(PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("en", "GB"));
	%>
<div class="formulari">
	<h1><%=etiquetes.getString("h1") %></h1>
	<form action="AddAlumnoController" method="post" onsubmit="return validaForm()">
	
		<label name="lblNom" id="lblNom"><%=etiquetes.getString("lblNom") %></label>
		<input name="nombre" type="text" id="nombre" class="inputs"/><br>
		
		<label name="lblCognoms" id="lblCognoms"><%=etiquetes.getString("lblCognoms") %></label>
		<input name="apellidos" type="text" id="apellidos" class="inputs"/><br>		
				
		<label name="lblEmpresa" id="lblEmpresa"><%=etiquetes.getString("lblEmpresa") %></label>
		<input name="empresa" type="text" id="empresa" class="inputs"/><br>
		
		<label name="lblTelf" id="lblTelf"><%=etiquetes.getString("lblTelf") %></label>
		<input name="telf" type="tel" id="telf" class="inputs" /><br>
		
		<label name="lblCorreu" id="lblCorreu"><%=etiquetes.getString("lblCorreu") %></label>
		<input name="correo" type="email" id="correo" class="inputs" /><br>
		
		<label name="lblCiutat" id="lblCiutat"><%=etiquetes.getString("lblCiutat") %></label>
		<input name="ciudad" type="text" id="ciudad" class="inputs"/><br>
		
		<label name="lblComentari" id="lblComentari"><%=etiquetes.getString("lblComentari") %></label>
		<textarea name="comentario" id="comentario" cols="60" rows="7"></textarea>
		
		<input type="checkbox" value="privacidad_valor" name="privacidad" id="privacidad"><%=etiquetes.getString("cbxPrivacitat") %><br>
		
		<button type="submit" name="enviar" id="enviar"><%=etiquetes.getString("btnEnviar") %></button>	
	</form>
</div>
<script src="formValidacion.js"></script>

</body>
</html>